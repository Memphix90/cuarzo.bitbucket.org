// Right_menu
$(document).ready(function() {
    $(".button_menu").click(function() {
        $(".black_background").addClass("active");
        $(".right_menu").addClass("active");
    });
    $(".off_bb").click(function() {
        $(".black_background").removeClass("active");
        $(".right_menu").removeClass("active");
    });
});

// Slider
var swiper_1 = new Swiper('.swiper-container-1', {
    effect: 'fade',
    loop: true,
    autoplay: 4000,
    nextButton: '.swiper-button-right-1',
    pagination: '.swiper-pagination-1',
    paginationClickable: true,
    prevButton: '.swiper-button-left-1'
});

// Gallery
var swiper_2 = new Swiper('.swiper-container-2', {
    effect: 'fade',
    loop: true,
    nextButton: '.swiper-button-right-2',
    pagination: '.swiper-pagination-2',
    paginationClickable: true,
    prevButton: '.swiper-button-left-2'
});

// Clients
$(document).ready(function() {
   var owl = $(".owl_demo");
   owl.owlCarousel({
      autoPlay: 3000,
      items : 6,
      itemsDesktop : [1199,3],
      itemsDesktopSmall : [979,3]
   });
   $(".owl_next").click(function(){
     owl.trigger('owl.next');
   })
   $(".owl_prev").click(function(){
     owl.trigger('owl.prev');
   })
});

// Review_repair
$(document).ready(function() {
   var owl_1 = $(".owl_demo_1");
   owl_1.owlCarousel({
      autoPlay: 4000,
      items : 3,
      itemsDesktop : [1199,3],
      itemsDesktopSmall : [979,3]
   });
});

// Review_workshop
$(document).ready(function() {
   var owl_2 = $(".owl_demo_2");
   owl_2.owlCarousel({
      autoPlay: 4000,
      items : 3,
      itemsDesktop : [1200,3],
      itemsDesktopSmall : [992,2],
      itemsTablet: [768,1],
      itemsMobile : false
   });
});

// Clean textarea
function eraseText() {
    document.getElementById("output").value = "";
}

// Fancybox
$(document).ready(function() {
	$(".fancybox").fancybox();
});

// Constructor
var swiper_3 = new Swiper('.swiper-container-3', {
	nextButton: '.next_slide',
	simulateTouch: false
});

$(document).ready(function() {
	$('.slide-1 a').on('click', function() {
		$(".slider_select_1 span.cross").css("display", "none");
		$(".slider_select_1 span.check").css("display", "table-cell");
	  	var input = $('input[name="name_1"]');
	  	input.val();
	  	input.val($(this).text());
	});

	$('.slide-2 a').on('click', function() {
		$(".slider_select_2 span.cross").css("display", "none");
		$(".slider_select_2 span.check").css("display", "table-cell");
	  	var input = $('input[name="name_2"]');
	  	input.val();
	  	input.val($(this).text());
	});

	$('.slide-3 a').on('click', function() {
		$(".slider_select_3 span.cross").css("display", "none");
		$(".slider_select_3 span.check").css("display", "table-cell");
	  	var input = $('input[name="name_3"]');
	  	input.val();
	  	input.val($(this).text());
	});

	$('.slide-4 a').on('click', function() {
		$(".slider_select_4 span.cross").css("display", "none");
		$(".slider_select_4 span.check").css("display", "table-cell");
	  	var input = $('input[name="name_4"]');
	  	input.val();
	  	input.val($(this).text());
	});


	$('.slide_skip_2').on('click', function() {
		$(".slider_select_2 span.cross").css("display", "table-cell");
		$(".slider_select_2 span.check").css("display", "none");
		var input = $('input[name="name_2"]');
	  	input.val();
	  	input.val($(this).text());
	});
	$('.slide_skip_3').on('click', function() {
		$(".slider_select_3 span.cross").css("display", "table-cell");
		$(".slider_select_3 span.check").css("display", "none");
		var input = $('input[name="name_3"]');
	  	input.val();
	  	input.val($(this).text());
	});
	$('.slide_skip_4').on('click', function() {
		$(".slider_select_4 span.cross").css("display", "table-cell");
		$(".slider_select_4 span.check").css("display", "none");
		var input = $('input[name="name_4"]');
	  	input.val();
	  	input.val($(this).text());
	});

	$('.slider_select_1').click(function(e) {
	    e.preventDefault();
	    swiper_3.slideTo( $('.slide-1').index(),1000,false );
	});
	$('.slider_select_2').click(function(e) {
	    e.preventDefault();
	    swiper_3.slideTo( $('.slide-2').index(),1000,false );
	});
	$('.slider_select_3').click(function(e) {
	    e.preventDefault();
	    swiper_3.slideTo( $('.slide-3').index(),1000,false );
	});
	$('.slider_select_4').click(function(e) {
	    e.preventDefault();
	    swiper_3.slideTo( $('.slide-4').index(),1000,false );
	});

	$(".catalog_button").click(function(e) {
        e.preventDefault();
	    swiper_3.slideTo( $('.slide-1').index(),1000,false );
	    $(".catalog_contsructor_slider_right input").val("");
	    $(".slider_select span.cross").css("display", "table-cell");
		$(".slider_select span.check").css("display", "none");
    });

	$(".catalog_button").click(function() {
        $(".catalog_contsructor_result").slideDown(350);
    });

});

var swiper_4 = new Swiper('.swiper-container-4', {
	nextButton: '.nnext_slide',
	simulateTouch: false
});

$(document).ready(function() {
	$('.sslide-1 a').on('click', function() {
		$(".sslider_select_1 span.cross").css("display", "none");
		$(".sslider_select_1 span.check").css("display", "table-cell");
	  	var input = $('input[name="nname_1"]');
	  	input.val();
	  	input.val($(this).text());
	});

	$('.sslide-2 a').on('click', function() {
		$(".sslider_select_2 span.cross").css("display", "none");
		$(".sslider_select_2 span.check").css("display", "table-cell");
	  	var input = $('input[name="nname_2"]');
	  	input.val();
	  	input.val($(this).text());
	});

	$('.sslide-3 a').on('click', function() {
		$(".sslider_select_3 span.cross").css("display", "none");
		$(".sslider_select_3 span.check").css("display", "table-cell");
	  	var input = $('input[name="nname_3"]');
	  	input.val();
	  	input.val($(this).text());
	});

	$('.sslide-4 a').on('click', function() {
		$(".sslider_select_4 span.cross").css("display", "none");
		$(".sslider_select_4 span.check").css("display", "table-cell");
	  	var input = $('input[name="nname_4"]');
	  	input.val();
	  	input.val($(this).text());
	});


	$('.sslide_skip_2').on('click', function() {
		$(".sslider_select_2 span.cross").css("display", "table-cell");
		$(".slider_select_2 span.check").css("display", "none");
		var input = $('input[name="nname_2"]');
	  	input.val();
	  	input.val($(this).text());
	});
	$('.sslide_skip_3').on('click', function() {
		$(".sslider_select_3 span.cross").css("display", "table-cell");
		$(".sslider_select_3 span.check").css("display", "none");
		var input = $('input[name="nname_3"]');
	  	input.val();
	  	input.val($(this).text());
	});
	$('.sslide_skip_4').on('click', function() {
		$(".sslider_select_4 span.cross").css("display", "table-cell");
		$(".sslider_select_4 span.check").css("display", "none");
		var input = $('input[name="nname_4"]');
	  	input.val();
	  	input.val($(this).text());
	});

	$('.sslider_select_1').click(function(e) {
	    e.preventDefault();
	    swiper_4.slideTo( $('.sslide-1').index(),1000,false );
	});
	$('.sslider_select_2').click(function(e) {
	    e.preventDefault();
	    swiper_4.slideTo( $('.sslide-2').index(),1000,false );
	});
	$('.sslider_select_3').click(function(e) {
	    e.preventDefault();
	    swiper_4.slideTo( $('.sslide-3').index(),1000,false );
	});
	$('.sslider_select_4').click(function(e) {
	    e.preventDefault();
	    swiper_4.slideTo( $('.sslide-4').index(),1000,false );
	});

	if($(".sslide-5").hasClass("swiper-slide-active")) {
	    $(".sslider_select_5 span.cross").css("display", "none");
		$(".sslider_select_5 span.check").css("display", "table-cell");
		$(".sslider_select_5 span.check").css("background", "#30b7f6");
	}else{
	    $(".sslider_select_5 span.cross").css("display", "table-cell");
		$(".sslider_select_5 span.check").css("display", "none");
	}

    $(".exclusive_button").click(function(e) {
        e.preventDefault();
	    swiper_4.slideTo( $('.sslide-1').index(),1000,false );
	    $(".exclusive_contsructor_slider_right input").val("");
	    $(".sl2 span.cross").css("display", "table-cell");
		$(".sl2 span.check").css("display", "none");
    });

});

